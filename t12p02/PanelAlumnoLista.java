/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t12p02;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;

/**
 *
 * @author Guillermo
 */
public class PanelAlumnoLista extends javax.swing.JPanel {

    private ConexionBD bd;
    private DefaultTableModel modeloTabla;

    public PanelAlumnoLista(ConexionBD bd) {
        this.bd = bd;
        initComponents();
        modeloTabla = new DefaultTableModel();
        listaAlumnosTabla.setModel(modeloTabla);
    }

    public PanelAlumnoLista() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tituloAlumnosLista = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaAlumnosTabla = new javax.swing.JTable();
        listaAlumnoAceptar = new javax.swing.JButton();
        listaAlumnoCancelar = new javax.swing.JButton();

        tituloAlumnosLista.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        tituloAlumnosLista.setText("Listado de Alumnos");

        jScrollPane1.setViewportView(listaAlumnosTabla);

        listaAlumnoAceptar.setText("Aceptar");
        listaAlumnoAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaAlumnoAceptarActionPerformed(evt);
            }
        });

        listaAlumnoCancelar.setText("Cancelar");
        listaAlumnoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaAlumnoCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addComponent(tituloAlumnosLista)
                .addContainerGap(65, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(listaAlumnoAceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(listaAlumnoCancelar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(tituloAlumnosLista)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(listaAlumnoAceptar)
                    .addComponent(listaAlumnoCancelar))
                .addContainerGap(58, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void listaAlumnoAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaAlumnoAceptarActionPerformed
        
        modeloTabla.setRowCount(0);
        List<Curso> tCursos = new ArrayList<>();
        List<Alumno> tAlumnos = new ArrayList<>();
        try {
            Curso.listadoCursos(bd, tCursos);
            Alumno.listadoAlumnos(bd, tAlumnos);
            String[] index = {"Curso","DNI","Nombre"};
            Collections.sort(tCursos);
            Collections.sort(tAlumnos);
            modeloTabla.setColumnCount(3);
            modeloTabla.setColumnIdentifiers(index);
            for (Curso c : tCursos) {
                for (Alumno a : tAlumnos) {
                    if (a.getIdCurso() == c.getId()) {
                        String[] datos={String.valueOf(a.getIdCurso()),a.getDni(),a.getNombre()};
                        modeloTabla.addRow(datos);
                    }
                }
                
            }
        } catch (Exception e) {
            System.out.println("Error!!\n" + e.getMessage());
        }


    }//GEN-LAST:event_listaAlumnoAceptarActionPerformed

    private void listaAlumnoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaAlumnoCancelarActionPerformed
        modeloTabla.setRowCount(0);
        this.setVisible(false);
    }//GEN-LAST:event_listaAlumnoCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton listaAlumnoAceptar;
    private javax.swing.JButton listaAlumnoCancelar;
    private javax.swing.JTable listaAlumnosTabla;
    private javax.swing.JLabel tituloAlumnosLista;
    // End of variables declaration//GEN-END:variables
}
