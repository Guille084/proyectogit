package t12p02;

import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;

public class PanelAlumnoAlta extends javax.swing.JPanel {

    private ConexionBD bd;

    public PanelAlumnoAlta(ConexionBD bd) {
        this.bd = bd;
        initComponents();
    }

    public PanelAlumnoAlta() {
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        tituloAlta = new javax.swing.JLabel();
        altaAlumnoLabCurso = new javax.swing.JLabel();
        altaAlumnoLabDNI = new javax.swing.JLabel();
        altaAlumnoLabNombre = new javax.swing.JLabel();
        altaAlumnoCurso = new javax.swing.JTextField();
        altaAlumnoDNI = new javax.swing.JTextField();
        altaAlumnoNombre = new javax.swing.JTextField();
        altaAlumnoAceptar = new javax.swing.JButton();
        altaAlumnoCancelar = new javax.swing.JButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jComboBox1 = new javax.swing.JComboBox<>();

        setMinimumSize(new java.awt.Dimension(480, 360));
        setPreferredSize(new java.awt.Dimension(480, 360));

        tituloAlta.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        tituloAlta.setText("Alta Alumnos");

        altaAlumnoLabCurso.setText("Curso:");

        altaAlumnoLabDNI.setText("DNI:");

        altaAlumnoLabNombre.setText("Nombre:");

        altaAlumnoAceptar.setText("Aceptar");
        altaAlumnoAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altaAlumnoAceptarActionPerformed(evt);
            }
        });

        altaAlumnoCancelar.setText("Cancelar");
        altaAlumnoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altaAlumnoCancelarActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("jRadioButton1");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("jRadioButton2");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(altaAlumnoAceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(altaAlumnoCancelar))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(altaAlumnoLabCurso)
                                        .addComponent(altaAlumnoLabDNI, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addComponent(altaAlumnoLabNombre))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tituloAlta)
                                    .addComponent(altaAlumnoDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(altaAlumnoCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(altaAlumnoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jRadioButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton2))
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(127, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(tituloAlta)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altaAlumnoLabCurso)
                    .addComponent(altaAlumnoCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altaAlumnoLabDNI)
                    .addComponent(altaAlumnoDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altaAlumnoLabNombre)
                    .addComponent(altaAlumnoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altaAlumnoAceptar)
                    .addComponent(altaAlumnoCancelar))
                .addGap(49, 49, 49))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void altaAlumnoAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altaAlumnoAceptarActionPerformed

        try {
            Curso c = new Curso();
            c.setId(Integer.parseInt(altaAlumnoCurso.getText()));  
            c.existeCurso(bd);    
            Alumno a = new Alumno();
            a.setIdCurso(c.getId());
            a.setDni(altaAlumnoDNI.getText());
            a.setNombre(altaAlumnoNombre.getText());
            a.altaAlumno(bd);
            
            JOptionPane.showMessageDialog(this,
                    "Alta de Alumno DNI: " + a.getDni() + " Correcta", "Alta de alumno",
                    JOptionPane.INFORMATION_MESSAGE);
            System.out.println("Alta de alumno correcta. DNI: " + a.getDni() + ".");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Alta de alumno: " + e.getMessage(), "Alta de alumno",
                    JOptionPane.ERROR_MESSAGE);
            System.out.println("Error!!\n" + e.getMessage());
        }
        for (Component C : this.getComponents()) {
            if (C instanceof JTextField) {
                ((JTextComponent) C).setText(""); //abstract superclass
            }
        }
        
        
    }//GEN-LAST:event_altaAlumnoAceptarActionPerformed

    private void altaAlumnoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altaAlumnoCancelarActionPerformed
        for (Component C : this.getComponents()) {
            if (C instanceof JTextField) {
                ((JTextComponent) C).setText(""); //abstract superclass
            }
        }
        this.setVisible(false);
    }//GEN-LAST:event_altaAlumnoCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton altaAlumnoAceptar;
    private javax.swing.JButton altaAlumnoCancelar;
    private javax.swing.JTextField altaAlumnoCurso;
    private javax.swing.JTextField altaAlumnoDNI;
    private javax.swing.JLabel altaAlumnoLabCurso;
    private javax.swing.JLabel altaAlumnoLabDNI;
    private javax.swing.JLabel altaAlumnoLabNombre;
    private javax.swing.JTextField altaAlumnoNombre;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JLabel tituloAlta;
    // End of variables declaration//GEN-END:variables
}
