package t12p02;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import modelo.ConexionBD;
import modelo.Curso;

/**
 *
 * @author Guillermo
 */
public class PanelCursoLista extends javax.swing.JPanel {

    private ConexionBD bd;
    private DefaultListModel modeloLista;

    public PanelCursoLista(ConexionBD bd) {
        this.bd = bd;
        initComponents();
        modeloLista = new DefaultListModel(); //Modelo para almacenar datos de la lista.
        listaCursoLista.setModel(modeloLista); //Variable que contiene el elemento grafico lista.
    }

    public PanelCursoLista() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tituloCursosLista = new javax.swing.JLabel();
        listaCursosCancelar = new javax.swing.JButton();
        bajaCursosAceptar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaCursoLista = new javax.swing.JList<>();

        tituloCursosLista.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        tituloCursosLista.setText("Listado de Cursos");

        listaCursosCancelar.setText("Cancelar");
        listaCursosCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaCursosCancelarActionPerformed(evt);
            }
        });

        bajaCursosAceptar.setText("Aceptar");
        bajaCursosAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaCursosAceptarActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(listaCursoLista);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(listaCursosCancelar)
                                .addGap(18, 18, 18)
                                .addComponent(bajaCursosAceptar))
                            .addComponent(tituloCursosLista, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(tituloCursosLista)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(listaCursosCancelar)
                    .addComponent(bajaCursosAceptar))
                .addContainerGap(39, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void listaCursosAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaCursosAceptarActionPerformed
        try {
            modeloLista.removeAllElements();
            List<Curso> tCursos = new ArrayList<>();
            Curso.listadoCursos(bd, tCursos);
            Collections.sort(tCursos);
            for (Curso c : tCursos) {
                modeloLista.addElement(String.format("ID: %-6d TÍTULO: %-10s HORAS: %6.2f\n",
                        c.getId(), c.getTitulo(), c.getHoras()));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "ERROR AL LISTAR: " + e.getMessage(), "Lista de cursos",
                    JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_listaCursosAceptarActionPerformed

    private void listaCursosCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaCursosCancelarActionPerformed
        modeloLista.removeAllElements();
        this.setVisible(false);
    }//GEN-LAST:event_listaCursosCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bajaCursosAceptar;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> listaCursoLista;
    private javax.swing.JButton listaCursosCancelar;
    private javax.swing.JLabel tituloCursosLista;
    // End of variables declaration//GEN-END:variables
}
