/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t12p02;

import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import modelo.ConexionBD;
import modelo.Curso;

/**
 *
 * @author Alumno
 */
public class PanelCursoAlta extends javax.swing.JPanel {

    private ConexionBD bd;

    public PanelCursoAlta(ConexionBD bd) {
        this.bd = bd;
        initComponents();
    }

    public PanelCursoAlta() {
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tituloAlta = new javax.swing.JLabel();
        altaCursosLabId = new javax.swing.JLabel();
        altaCursosLabTitulo = new javax.swing.JLabel();
        altaCursosLabHoras = new javax.swing.JLabel();
        altaCursosId = new javax.swing.JTextField();
        altaCursosTitulo = new javax.swing.JTextField();
        altaCursosHoras = new javax.swing.JTextField();
        altaCursosCancelar = new javax.swing.JButton();
        altaCursosAceptar = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(400, 300));
        setMinimumSize(new java.awt.Dimension(400, 300));
        setPreferredSize(new java.awt.Dimension(400, 300));

        tituloAlta.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        tituloAlta.setText("Alta Cursos");
        tituloAlta.setMinimumSize(new java.awt.Dimension(380, 240));

        altaCursosLabId.setText("Identificacion:");

        altaCursosLabTitulo.setText("Titulo:");

        altaCursosLabHoras.setText("Horas:");

        altaCursosCancelar.setText("Cancelar");
        altaCursosCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altaCursosCancelarActionPerformed(evt);
            }
        });

        altaCursosAceptar.setText("Aceptar");
        altaCursosAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altaCursosAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(110, 110, 110)
                        .addComponent(tituloAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(altaCursosLabId)
                        .addGap(18, 18, 18)
                        .addComponent(altaCursosId, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(altaCursosLabTitulo)
                        .addGap(56, 56, 56)
                        .addComponent(altaCursosTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(altaCursosLabHoras)
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(altaCursosCancelar)
                                .addGap(18, 18, 18)
                                .addComponent(altaCursosAceptar))
                            .addComponent(altaCursosHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(tituloAlta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(altaCursosLabId))
                    .addComponent(altaCursosId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(altaCursosLabTitulo))
                    .addComponent(altaCursosTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(altaCursosLabHoras))
                    .addComponent(altaCursosHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(altaCursosCancelar)
                    .addComponent(altaCursosAceptar))
                .addGap(13, 13, 13))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void altaCursosAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altaCursosAceptarActionPerformed

        try {
            Curso c = new Curso();
            c.setId(Integer.parseInt(altaCursosId.getText()));
            c.setTitulo(altaCursosTitulo.getText());
            c.setHoras(Double.parseDouble(altaCursosHoras.getText()));
            c.altaCurso(bd);
            JOptionPane.showMessageDialog(this,
                    "Alta de curso Id: "+c.getId()+"Correcta", "Alta de curso",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Alta de curso: " + e.getMessage(), "Alta de curso",
                    JOptionPane.ERROR_MESSAGE);
        }
        for (Component C : this.getComponents()) {
            if (C instanceof JTextField) {
                ((JTextComponent) C).setText("");
            }
        }
    }//GEN-LAST:event_altaCursosAceptarActionPerformed

    private void altaCursosCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altaCursosCancelarActionPerformed
        for (Component C : getComponents()) {
            if (C instanceof JTextField) {
                ((JTextComponent) C).setText("");
            }
        }
        this.setVisible(false);

    }//GEN-LAST:event_altaCursosCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton altaCursosAceptar;
    private javax.swing.JButton altaCursosCancelar;
    private javax.swing.JTextField altaCursosHoras;
    private javax.swing.JTextField altaCursosId;
    private javax.swing.JLabel altaCursosLabHoras;
    private javax.swing.JLabel altaCursosLabId;
    private javax.swing.JLabel altaCursosLabTitulo;
    private javax.swing.JTextField altaCursosTitulo;
    private javax.swing.JLabel tituloAlta;
    // End of variables declaration//GEN-END:variables
}
