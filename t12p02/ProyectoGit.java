package t12p02;

import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ProyectoGit
{
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int op;
        ConexionBD bd=new ConexionBD();
        
        /* ABRIR CONEXIÓN BD **************************************************/
        try {
            System.out.println("Abriendo conexión BD...");
            bd.abrirConexion();
            System.out.println("Conexión abierta correctamente.");
        } catch (Exception e) {
            System.out.println("Error!!\n"+e.getMessage());
        }
        /**********************************************************************/
        
        do
        {
            System.out.println("");
            System.out.println("Seleccionar una opción:");
            System.out.println("-----------------------");
            System.out.println();
            System.out.println("1.- Alta de Curso.");
            System.out.println("2.- Baja de Curso.");
            System.out.println("3.- Alta de Alumno.");
            System.out.println("4.- Baja de Alumno.");
            System.out.println("5.- Listado de Cursos y Alumnos.");
            System.out.println("0.- Salir.");
            System.out.println();
            System.out.print("Opción? ");
            op=sc.nextInt();
            System.out.println("");

            switch (op)
            {
            case 1: // Alta de Curso
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                System.out.print("Introduce el título del curso: ");
                c.setTitulo(sc.next());
                System.out.print("Introduce las horas del curso: ");
                c.setHoras(sc.nextDouble());
                System.out.println("");
                try {
                    c.altaCurso(bd);
                    System.out.println("Alta de curso correcta. ID: "+c.getId()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 2: // Baja de Curso
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                System.out.println("");
                try {
                    c.bajaCurso(bd);
                    //c.bajaCursoSinCascade(bd);
                    System.out.println("Baja de curso correcta. ID: "+c.getId()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 3: // Alta de Alumno
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                try {
                    if (!c.existeCurso(bd)) {
                        System.out.println("El curso no existe!!");
                        break;
                    }
                    Alumno a=new Alumno();
                    a.setIdCurso(c.getId());
                    System.out.print("Introduce el dni del alumno: ");
                    a.setDni(sc.next());
                    System.out.print("Introduce el nombre del alumno: ");
                    a.setNombre(sc.next());
                    System.out.println("");
                    a.altaAlumno(bd);
                    System.out.println("Alta de alumno correcta. DNI: "+a.getDni()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 4: // Baja de Alumno
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                try {
                    if (!c.existeCurso(bd)) {
                        System.out.println("El curso no existe!!");
                        break;
                    }
                    Alumno a=new Alumno();
                    a.setIdCurso(c.getId());
                    System.out.print("Introduce el dni del alumno: ");
                    a.setDni(sc.next());
                    System.out.println("");
                    a.bajaAlumno(bd);
                    System.out.println("Baja de alumno correcta. DNI: "+a.getDni()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 5: // Listado de Cursos y Alumnos
            {
                System.out.println("LISTADO DE CURSOS Y ALUMNOS");
                System.out.println("---------------------------");
                List<Curso> tCursos=new ArrayList<>();
                List<Alumno> tAlumnos=new ArrayList<>();
                try {
                    Curso.listadoCursos(bd,tCursos);
                    Alumno.listadoAlumnos(bd, tAlumnos);
                    Collections.sort(tCursos);
                    Collections.sort(tAlumnos);
                    for (Curso c: tCursos) {
                        System.out.printf("ID: %-6d TÍTULO: %-10s HORAS: %6.2f\n",
                                            c.getId(),c.getTitulo(),c.getHoras());
                        for (Alumno a: tAlumnos)
                        {
                            if (a.getIdCurso()==c.getId())
                                System.out.printf("\tDNI: %-10s NOMBRE: %-10s\n",
                                                    a.getDni(),a.getNombre());
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }                
                break;
            }   
            case 0: // Salir
            {
                /* CERRAR CONEXIÓN BD *****************************************/
                try {
                    System.out.println("Cerrando conexión BD...");
                    bd.cerrarConexion();
                    System.out.println("Conexión cerrada correctamente.");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                /**************************************************************/
                break;
            }
            default:
            {
                System.out.println("Opción incorrecta!!");
                System.out.println("");
            }
            }
        }
        while (op!=0);
        System.out.println("");
    
    }
    
}
