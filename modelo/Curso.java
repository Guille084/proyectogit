package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Curso implements Comparable<Curso> {

    /* Atributos **************************************************************/
    private int id;
    private String titulo;
    private double horas;
    private LocalDate fecIni;
    private LocalDate fecFin;
    private char modalidad;
    private ESTADOCURSO estado;

    public enum ESTADOCURSO {
        Programado, Realizandose, Finalizado
    };

    /* Constructores **********************************************************/
    public Curso() {
        id = 0;
        titulo = "";
        horas = 0.0;
        fecIni = null;
        fecFin = null;
        modalidad = 'P';
        estado = ESTADOCURSO.Programado;
    }

    /* Métodos getters & setters **********************************************/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getHoras() {
        return horas;
    }

    public void setHoras(double horas) {
        this.horas = horas;
    }

    public String getFecIni() {
        if (fecIni != null) {
            return fecIni.format(DateTimeFormatter.ofPattern("dd/MM/uuuu"));
        } else {
            return "";
        }
    }

    public void setFecIni(String fecIni) {
        if (fecIni.equals("0")) {
            return;
        }
        this.fecIni = LocalDate.of(Integer.parseInt(fecIni.substring(6, 10)),
                Integer.parseInt(fecIni.substring(3, 5)),
                Integer.parseInt(fecIni.substring(0, 2)));
    }

    public String getFecFin() {
        if (fecFin != null) {
            return fecFin.format(DateTimeFormatter.ofPattern("dd/MM/uuuu"));
        } else {
            return "";
        }
    }

    public void setFecFin(String fecFin) {
        if (fecFin.equals("0")) {
            return;
        }
        this.fecFin = LocalDate.of(Integer.parseInt(fecFin.substring(6, 10)),
                Integer.parseInt(fecFin.substring(3, 5)),
                Integer.parseInt(fecFin.substring(0, 2)));
    }

    public char getModalidad() {
        return modalidad;
    }

    public void setModalidad(char modalidad) {
        this.modalidad = modalidad;
    }

    public ESTADOCURSO getEstado() {
        return estado;
    }

    public void setEstado(ESTADOCURSO estado) {
        this.estado = estado;
    }

    /* Métodos ****************************************************************/
    public boolean existeCurso(ConexionBD bd) throws Exception {
        try {
            String sql = "SELECT count(*) FROM Cursos WHERE "
                    + "id=" + id;
            ResultSet rs = bd.getSt().executeQuery(sql);
            rs.next();
            int n = rs.getInt(1);
            if (n > 0) {
                return true;
            }
        } catch (SQLException e) {
            throw new Exception("Error existeCurso()!!", e);
        }
        return false;
    }

    public void altaCurso(ConexionBD bd) throws Exception {
        if (existeCurso(bd)) {
            throw new Exception("El curso ya existe!!");
        }
        try {
            String sql = "INSERT INTO Cursos VALUES ("
                    + id + ",'"
                    + titulo + "',"
                    + horas + ")";
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error altaCurso()!!", e);
        }
    }

    public void bajaCurso(ConexionBD bd) throws Exception {
        if (!existeCurso(bd)) {
            throw new Exception("El curso no existe!!");
        }
        try {
            String sql = "DELETE FROM Cursos WHERE "
                    + "id=" + id;
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error bajaCurso()!!", e);
        }
    }

    public void bajaCursoSinCascade(ConexionBD bd) throws Exception {
        if (!existeCurso(bd)) {
            throw new Exception("El curso no existe!!");
        }
        try {
            bd.getConn().setAutoCommit(false);
            String sql = "DELETE FROM Alumnos WHERE "
                    + "idCurso=" + id;
            bd.getSt().executeUpdate(sql);
            sql = "DELETE FROM Cursos WHERE "
                    + "id=" + id;
            bd.getSt().executeUpdate(sql);
            bd.getConn().commit();
        } catch (SQLException e) {
            bd.getConn().rollback();
            throw new Exception("Error bajaCurso()!!", e);
        } finally {
            bd.getConn().setAutoCommit(true);
        }
    }

    public static void listadoCursos(ConexionBD bd, List<Curso> t) throws Exception {
        try {
            String sql = "SELECT * FROM Cursos";
            ResultSet rs = bd.getSt().executeQuery(sql);
            Curso c;
            while (rs.next()) {
                c = new Curso();
                c.setId(rs.getInt("id"));
                c.setTitulo(rs.getString("titulo"));
                c.setHoras(rs.getDouble("horas"));
                t.add(c);
            }
        } catch (SQLException e) {
            throw new Exception("Error listadoCursos()!!", e);
        }
    }

    @Override
    public int compareTo(Curso o) {
        if (this.getId() == o.getId()) {
            return 0;
        } else if (this.getId() > o.getId()) {
            return 1;
        } else {
            return -1;
        }
    }

}
